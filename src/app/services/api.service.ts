import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

export interface GeoData {
  latitude: number;
  longitude: number;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl = 'https://api.openweathermap.org/data/2.5/';
  private appId = '7d3139fb28f804b085767810aa67e424';

  constructor(
    private httpClient: HttpClient
  ) { }

  public getByCityName(city: string) {
    return this.httpClient.get(`${this.apiUrl}weather?q=${city}&units=metric&APPID=${this.appId}`);
  }

  public getByGeoCoordinates(data: GeoData) {
    return this.httpClient.get(`${this.apiUrl}weather?lat=${data.latitude}&lon=${data.longitude}&units=metric&APPID=${this.appId}`);
  }

  public find(name: string) {
    return this.httpClient.get(`${this.apiUrl}find?q=${name}&units=metric&APPID=${this.appId}`).pipe(
      map((data: any) => {
        const citiesList = data.list.sort((city1, city2) => {
          if (city1.sys.country > city2.sys.country) {
            return -1;
          } else if (city1.sys.country < city2.sys.country) {
            return 1;
          } else {
            return 0;
          }
        });
        return citiesList;
      })
    );
  }


}
